# Configure the GitLab Provider
terraform {
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
        }
    }
}