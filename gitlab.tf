# Provider GitLab
provider "gitlab" {
  token = var.access_token
}

# Resource to create project
resource "gitlab_project" "sample_project" {
  name = var.project_name
  wiki_enabled = var.enabled_wiki
  initialize_with_readme = var.initialize_with_readme
  monitor_access_level = var.monitor_access_level
  issues_enabled = var.issues_enabled
  namespace_id = var.project_group_id
  default_branch = var.default_branch
  description = var.project_description
}


# Resource to create project (https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection#nestedblock--allowed_to_merge)
resource "gitlab_branch_protection" "master" {
  project = "${gitlab_project.sample_project.id}"
  branch = var.default_branch
  merge_access_level = "maintainer"
  push_access_level = "no one"
  
}
