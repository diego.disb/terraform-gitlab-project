## POC TERRAFORM

### Terraform-Gitlab-Project

1. Direccion repositorio
```
# https://gitlab.com/diego.disb/terraform-gitlab-project.git--create-namespace
```
2. Pipeline .gitlab-ci.yml
```
variables:  
  TF_VAR_project_name:
        value: ""
        description: "Project name"

  TF_VAR_enabled_wiki:
        value: ""
        description: "Enabled Wiki true / false"
        
  TF_VAR_access_token: "${access_token}"
  TF_VAR_provider_source_gitlab: "${provider_source}"

include:
  - template: Terraform/Base.gitlab-ci.yml 

stages:
  - validate
  - test
  - build
  - deploy
  - cleanup

validate:
  extends: .terraform:validate
  needs: []

build:
  extends: .terraform:build
  environment:
    name: $TF_STATE_NAME
    action: prepare

deploy:
  extends: .terraform:deploy
  dependencies:
    - build
  environment:
    name: $TF_STATE_NAME
    action: start

```
3. Manifiesto Terraform providers.tf
```
# Configure the GitLab Provider
terraform {
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
        }
    }
}
```
4. Manifiesto Terraform gitlab.tf
```
provider "gitlab" {
  token = var.access_token
}

resource "gitlab_project" "sample_project" {
  name = var.project_name
  wiki_enabled = var.enabled_wiki
}

```
5. Manifiesto Terraform variables.tf
```
variable "provider_source_gitlab" {
  type        = string
  default     = ""
  description = "Configure the GitLab Provider."
}

variable "access_token" {
  type        = string
  default     = ""
  description = "Configure access control token."
}

variable "project_name" {
  type        = string
  default     = ""
  description = "Configure project created name."
}

variable "enabled_wiki" {
  type        = string
  default     = ""
  description = "Enabled Wiki true / false."
}

```
### Explicacion Manifiesto pipeline

El pipeline usa una variables de Proyecto:
```
access_token
provider_source_gitlab
```
El pipeline usa una variables de entrada en el Pipeline:
```
project_name
enabled_wiki
```
Al momento de utilizar las variables en el manifiesto, y para que Terraform los ocupe se debe anteponer el prefijo TF_VAR_ . Quedando asi las variables en el manifiesto de la siguiente forma:
```
TF_VAR_project_name
TF_VAR_enabled_wiki
TF_VAR_access_token
TF_VAR_provider_source_gitlab
```
Los Stage del manifiesto son:
```
  - validate
  - build
  - deploy
```
Dentro de las funcionalidades de Terraform se encuentran los archivos
```
  - providers.tf : Brinda el provider de Gitlab.
  - gitlab.tf :    Manifiesto para el control de la tarea.
  - variables.tf : Brianda el control de las variables que interactua con Terraform.
```