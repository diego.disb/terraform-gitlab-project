variable "provider_source_gitlab" {
  type        = string
  default     = "gitlabhq/gitlab"
  description = "Configure the GitLab Provider."
}

variable "access_token" {
  type        = string
  default     = ""
  description = "Configure access control token."
}

variable "project_name" {
  type        = string
  default     = ""
  description = "Configure project created name."
}

variable "enabled_wiki" {
  type        = string
  default     = ""
  description = "Enabled Wiki: true / false."
}

variable "project_group_id" {
  type        = string
  default     = ""
  description = "ID Group (Numeric)"
}

variable "initialize_with_readme" {
  type        = string
  default     = ""
  description = "Inicialize with readme: true / false."
}

variable "monitor_access_level" {
  type        = string
  default     = ""
  description = "Monitor access level: disabled / private / enabled."
}

variable "issues_enabled" {
  type        = string
  default     = ""
  description = "Issues enabled: true / false."
}

variable "default_branch" {
  type        = string
  default     = "master"
  description = "The default branch for the project."
}

variable "project_description" {
  type        = string
  default     = ""
  description = "Description of the project."
}